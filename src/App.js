import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      firstValue: '',
      condition: 'cond',
      secondValue: '',
      value: ''
    }
    console.log(props.value);
    this.handleClick = this.handleClick.bind(this);
  }

  renderSquare(i) {
    return (
      <button className="square" onClick={() => this.handleClick(i)}>
        {i}
      </button>
    );
  }

  componentDidUpdate() {

  }

  componentWillUpdate() {

  }

  handleClick(props) {
    if (props === 'AC') {
      this.setState(prevState => ({
        value: '',
        firstValue: '',
        secondValue: '',
        condition: 'cond'
      }));
    } else if (props === '+' || props === '-' || props === '*' || props === '/' || props === '%') {
      this.setState(prevState => ({
        condition: props + "",
        firstValue: prevState.value,
        value: ''
      }));
    } else if (props === '=') {
      if (this.state.secondValue !== 'cond')
        switch (this.state.condition) {
          case '+':
            this.setState(prevState => ({
              value: parseInt(prevState.firstValue, 10) + parseInt(prevState.secondValue, 10) + "",
              firstValue: prevState.value,
              secondValue: '',
              condition: 'cond'
            }));
            break;
          case '-':
            this.setState(prevState => ({
              value: parseInt(prevState.firstValue, 10) - parseInt(prevState.secondValue, 10) + "",
              firstValue: prevState.value,
              secondValue: '',
              condition: 'cond'
            }));
            break;
          case '*':
            this.setState(prevState => ({
              value: parseInt(prevState.firstValue, 10) * parseInt(prevState.secondValue, 10) + "",
              firstValue: prevState.value,
              secondValue: '',
              condition: 'cond'
            }));
            break;
          case '/':
            this.setState(prevState => ({
              value: parseInt(prevState.firstValue, 10) / parseInt(prevState.secondValue, 10) + "",
              firstValue: prevState.value,
              secondValue: '',
              condition: 'cond'
            }));
            break;
          case '%':
            this.setState(prevState => ({
              value: parseInt(prevState.firstValue, 10) % parseInt(prevState.secondValue, 10) + "",
              firstValue: prevState.value,
              secondValue: '',
              condition: 'cond'
            }));
            break;
        }
    } else if (parseInt(props, 10) >= 0 || parseInt(props, 10) <= 9) {
      console.log(this.state.condition);
      console.log(props);
      if (this.state.condition !== 'cond') {
        this.setState(prevState => ({
          secondValue: prevState.secondValue + props + "",
          value: prevState.secondValue + props + "",
        }));
      } else {
        this.setState(prevState => ({
          firstValue: prevState.firstValue + props + "",
          value: prevState.firstValue + props + ""
        }));
      }
    }

    // console.log(props + "");
    return null;
  }

  renderValue(i) {
    return (<form onSubmit={this.handleSubmit}>
      <input className="value-output" value={this.state.value} type="text" />
    </form>);
  }

  render() {
    return (
      <div className="App">
        <div className="board-row">
          {this.renderValue('')}
          {this.renderSquare('=')}
        </div>
        <div className="board-row">
          {this.renderSquare('7')}
          {this.renderSquare('8')}
          {this.renderSquare('9')}
          {this.renderSquare('%')}
        </div>
        <div className="board-row">
          {this.renderSquare('4')}
          {this.renderSquare('5')}
          {this.renderSquare('6')}
          {this.renderSquare('/')}
        </div>
        <div className="board-row">
          {this.renderSquare('1')}
          {this.renderSquare('2')}
          {this.renderSquare('3')}
          {this.renderSquare('*')}
        </div>
        <div className="board-row">
          {this.renderSquare('AC')}
          {this.renderSquare('0')}
          {this.renderSquare('+')}
          {this.renderSquare('-')}
        </div>
      </div>
    );

  }

}


export default App;
